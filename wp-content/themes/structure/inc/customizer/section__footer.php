<?php
/**
 * ============================================================================
 * Create sections: Footer settings
 * ============================================================================
 */
function register_sections_footer_settings( $wp_customize ) {
	$wp_customize->add_section( 'footer_settings_section', array(
		'title'       => esc_html__( 'Footer', 'structure' ),
		'description' => esc_html__( 'Scroll to bottom of page to see the change', 'structure' ),
		'priority'    => 15,
	) );
}

add_action( 'customize_register', 'register_sections_footer_settings' );
/**
 * ============================================================================
 * Create controls for section: footer settings
 * ============================================================================
 */
function register_controls_for_footer_settings_section( $controls ) {

	$section  = 'footer_settings_section';
	$priority = 1;

	//Uncovering Footer
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'footer_uncovering_enable',
		'label'     => esc_html__( 'Uncovering', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will make Footer gradually appear on scroll', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'default'   => footer_uncovering_enable,
		'priority'  => $priority ++
	);

	//Copyright Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'site_group_title_footer_copyright',
		'label'     => esc_html__( 'Copyright', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Copyright
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'footer_copyright_enable',
		'subtitle'  => esc_html__( 'Enabling this option will display copyright info', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'default'   => footer_copyright_enable,
		'priority'  => $priority ++
	);

	//Copyright Text
	$controls[] = array(
		'type'        => 'textarea',
		'setting'     => 'copyright_text',
		'label'       => esc_html__( 'Copyright Text', 'structure' ),
		'section'     => $section,
		'placeholder' => esc_html__( 'Entry your custom css code here', 'structure' ),
		'default'     => esc_html__( 'Copyright 2014 ThemeMove.com. All right reserved.', 'structure' ),
		'priority'    => $priority ++
	);

	return $controls;
}

add_filter( 'kirki/controls', 'register_controls_for_footer_settings_section' );
