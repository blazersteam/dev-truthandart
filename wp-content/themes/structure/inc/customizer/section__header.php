<?php
/**
 * ============================================================================
 * Create sections: Header settings
 * ============================================================================
 */

function register_sections_header_settings( $wp_customize ) {
	$wp_customize->add_section( 'header_settings_section', array(
		'title'       => esc_html__( 'Header', 'structure' ),
		'description' => esc_html__( 'In this section you can control all header settings of your site', 'structure' ),
		'priority'    => 10,
	) );
}

add_action( 'customize_register', 'register_sections_header_settings' );
/**
 * ============================================================================
 * Create controls for section: header settings
 * ============================================================================
 */
function register_controls_for_header_settings_section( $controls ) {

	$section  = 'header_settings_section';
	$priority = 1;

	//Header Presets Settings Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'group_title_header_presets',
		'label'     => esc_html__( 'Header Presets', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Header Presets choose
	$controls[] = array(
		'type'      => 'select',
		'setting'   => 'header_preset',
		'subtitle'  => esc_html__( 'Choose a preset setup for your header', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'default'   => header_preset,
		'choices'   => array(
			'header-preset-01' => esc_html__( 'Preset 01', 'structure' ),
			'header-preset-02' => esc_html__( 'Preset 02', 'structure' ),
			'header-preset-03' => esc_html__( 'Preset 03', 'structure' ),
			'header-preset-04' => esc_html__( 'Preset 04', 'structure' ),
			'header-preset-05' => esc_html__( 'Preset 05', 'structure' ),
			'header-preset-06' => esc_html__( 'Preset 06', 'structure' ),
			'header-preset-07' => esc_html__( 'Preset 07', 'structure' ),
			'header-preset-08' => esc_html__( 'Preset 08', 'structure' ),
		),
		'priority'  => $priority ++
	);

	//Header Presets Settings Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'group_title_header_general_settings',
		'label'     => esc_html__( 'General Settings', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Header Top Area
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'header_top_enable',
		'label'     => esc_html__( 'Top Area', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will show header top area', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => header_top_enable,
		'priority'  => $priority ++
	);

	//Header Sticky
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'header_sticky_enable',
		'label'     => esc_html__( 'Sticky', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will sticky your header', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => header_sticky_enable,
		'priority'  => $priority ++
	);

	//Header Search
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'header_search_enable',
		'label'     => esc_html__( 'Search', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will display search button on header', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => header_search_enable,
		'priority'  => $priority ++
	);

	//Header Search
	$controls[] = array(
		'type'     => 'checkbox',
		'mode'     => 'toggle',
		'setting'  => 'header_cart_enable',
		'label'    => esc_html__( 'Mini Cart', 'structure' ),
		'subtitle' => esc_html__( 'Enabling this option will display mini cart button on header', 'structure' ),
		'section'  => $section,
		'default'  => header_cart_enable,
		'priority' => $priority ++
	);

	//Normal Logo Settings
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'site_group_title_normal_logo_settings',
		'label'     => esc_html__( 'Logo', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Normal Logo Image
	$controls[] = array(
		'type'        => 'image',
		'setting'     => 'normal_logo_image',
		'label'       => esc_html__( 'Logo Image - Normal', 'structure' ),
		'description' => esc_html__( 'Choose a default logo image to display', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => normal_logo_image,
		'priority'    => $priority ++
	);

	return $controls;
}

add_filter( 'kirki/controls', 'register_controls_for_header_settings_section' );
