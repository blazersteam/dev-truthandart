<?php
/**
 * ============================================================================
 * Create sections: Options
 * ============================================================================
 */
function register_sections_site_options( $wp_customize ) {
	$wp_customize->add_section( 'site_settings_section', array(
		'title'       => esc_html__( 'General', 'structure' ),
		'description' => esc_html__( 'In this section you can control all general settings of your site', 'structure' ),
		'priority'    => 1,
	) );
}

add_action( 'customize_register', 'register_sections_site_options' );
/**
 * ============================================================================
 * Create controls for section: site settings
 * ============================================================================
 */
function register_controls_for_site_settings_section( $controls ) {

	$section  = 'site_settings_section';
	$priority = 4;

	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'group_title_site_homepage',
		'label'     => esc_html__( 'Front Page', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => 1
	);

	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'group_title_site_general_settings',
		'label'     => esc_html__( 'General Settings', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Box Mode
	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'box_mode_enable',
		'label'     => esc_html__( 'Box Mode', 'structure' ),
		'subtitle'  => esc_html__( 'Turn on this option if you want to enable box mode on your site', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => box_mode_enable,
		'priority'  => $priority ++
	);

	//Page Layout
	$controls[] = array(
		'type'      => 'radio',
		'mode'      => 'image',
		'setting'   => 'site_layout',
		'label'     => esc_html__( 'Page Layout', 'structure' ),
		'subtitle'  => esc_html__( 'Choose the page layout you want', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => site_layout,
		'choices'   => array(
			'full-width'      => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/1c.png',
			'content-sidebar' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cr.png',
			'sidebar-content' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cl.png',
		),
		'priority'  => $priority ++
	);

	//Search Layout
	$controls[] = array(
		'type'      => 'radio',
		'mode'      => 'image',
		'setting'   => 'search_layout',
		'label'     => esc_html__( 'Search Layout', 'structure' ),
		'subtitle'  => esc_html__( 'Choose the layout for search page', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => search_layout,
		'choices'   => array(
			'full-width'      => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/1c.png',
			'content-sidebar' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cr.png',
			'sidebar-content' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cl.png',
		),
		'priority'  => $priority ++
	);

	//Archive Layout
	$controls[] = array(
		'type'      => 'radio',
		'mode'      => 'image',
		'setting'   => 'archive_layout',
		'label'     => esc_html__( 'Archive Layout', 'structure' ),
		'subtitle'  => esc_html__( 'Choose the layout for archive page', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => archive_layout,
		'choices'   => array(
			'full-width'      => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/1c.png',
			'content-sidebar' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cr.png',
			'sidebar-content' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cl.png',
		),
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'select',
		'setting'   => 'page_transition',
		'label'     => esc_html__( 'Page Transition', 'structure' ),
		'subtitle'  => esc_html__( 'Choose a transition effect for your page', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'choices'   => array(
			'none'  => 'None',
			'type1' => 'Type 1',
			'type2' => 'Type 2',
		),
		'default'   => 'none',
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'enable_smooth_scroll',
		'label'     => esc_html__( 'Smooth Scroll', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will perform a smooth scrolling effect on every page', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => enable_smooth_scroll,
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'custom_scrollbar_enable',
		'label'     => esc_html__( 'Scroll bar', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option use custom color for scroll bar', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => custom_scrollbar_enable,
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'enable_back_to_top',
		'label'     => esc_html__( 'Back To Top', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will display a Back to Top button on every page', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => enable_back_to_top,
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'        => 'image',
		'setting'     => 'default_heading_image',
		'label'       => esc_html__( 'Default Heading Background', 'structure' ),
		'description' => esc_html__( 'Default background image for heading title', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => default_heading_image,
		'priority'    => $priority ++
	);

	//Favicon Settings Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'site_group_title_favicon_settings',
		'label'     => esc_html__( 'Favicon', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Favicon Image
	$controls[] = array(
		'type'        => 'image',
		'setting'     => 'favicon_image',
		'label'       => esc_html__( 'Favicon Image', 'structure' ),
		'description' => esc_html__( 'File must be .png or .ico format. Optimal dimensions: 32px x 32px.', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => favicon_image,
		'priority'    => $priority ++
	);

	//Apple Touch Icon
	$controls[] = array(
		'type'        => 'image',
		'setting'     => 'apple_touch_icon',
		'label'       => esc_html__( 'Apple Touch Icon', 'structure' ),
		'description' => esc_html__( 'File must be .png format. Optimal dimensions: 152px x 152px.', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => apple_touch_icon,
		'priority'    => $priority ++
	);

	return $controls;
}

add_filter( 'kirki/controls', 'register_controls_for_site_settings_section' );
