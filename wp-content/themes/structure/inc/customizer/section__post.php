<?php
/**
 * ============================================================================
 * Create sections: Post settings
 * ============================================================================
 */
function register_sections_post_settings( $wp_customize ) {
	$wp_customize->add_section( 'post_settings_section', array(
		'title'    => esc_html__( 'Post', 'structure' ),
		'priority' => 16,
	) );
}

add_action( 'customize_register', 'register_sections_post_settings' );
/**
 * ============================================================================
 * Create controls for section: footer settings
 * ============================================================================
 */
function register_controls_for_post_settings_section( $controls ) {

	$section  = 'post_settings_section';
	$priority = 1;

	//Site Layout
	$controls[] = array(
		'type'      => 'radio',
		'mode'      => 'image',
		'setting'   => 'post_layout',
		'label'     => esc_html__( 'Layout', 'structure' ),
		'subtitle'  => esc_html__( 'Choose the post layout you want', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => post_layout,
		'choices'   => array(
			'full-width'      => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/1c.png',
			'content-sidebar' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cr.png',
			'sidebar-content' => STRUCTURE_THEME_ROOT . '/core/customizer/assets/images/2cl.png',
		),
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'post_meta_enable',
		'label'     => esc_html__( 'Post Author Box', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will display author box at bottom of post', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => post_meta_enable,
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'checkbox',
		'mode'      => 'toggle',
		'setting'   => 'post_feature_image_enable',
		'label'     => esc_html__( 'Feature Image', 'structure' ),
		'subtitle'  => esc_html__( 'Enabling this option will display big feature image at top of post', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => post_feature_image_enable,
		'priority'  => $priority ++
	);

	$controls[] = array(
		'type'      => 'text',
		'setting'   => 'post_heading_text',
		'label'     => esc_html__( '"Our Blog" text', 'structure' ),
		'subtitle'  => esc_html__( 'Choose the text for single post heading', 'structure' ),
		'section'   => $section,
		'separator' => true,
		'default'   => post_heading_text,
		'priority'  => $priority ++
	);

	return $controls;
}

add_filter( 'kirki/controls', 'register_controls_for_post_settings_section' );
