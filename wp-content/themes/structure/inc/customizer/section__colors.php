<?php
/**
 * ============================================================================
 * Create sections: Colors
 * ============================================================================
 */
function register_sections_color( $wp_customize ) {
	$wp_customize->add_section( 'site_colors_section', array(
		'title'    => esc_html__( 'Colors', 'structure' ),
		'priority' => 20,
	) );
}

add_action( 'customize_register', 'register_sections_color' );
/**
 * ============================================================================
 * Create controls for site colors section
 * ============================================================================
 */
function register_controls_for_site_colors_section( $controls ) {
	$color_scheme = thememove_get_color_scheme();
	$section      = 'site_colors_section';
	$priority     = 1;

	//Site Color Scheme choose
	$controls[] = array(
		'type'      => 'select',
		'setting'   => 'site_color_scheme',
		'label'     => esc_html__( 'Color Scheme', 'structure' ),
		'subtitle'  => esc_html__( 'Choose a color scheme for your site', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'default'   => 'default',
		'choices'   => thememove_get_color_scheme_choices(),
		'priority'  => $priority ++
	);

	//Site Color Scheme Settings Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'group_title_site_color_scheme',
		'label'     => esc_html__( 'Site Color', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Primary Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'primary_color',
		'label'       => esc_html__( 'Primary Color', 'structure' ),
		'description' => esc_html__( 'Choose the most dominant theme color', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[0],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Secondary Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'secondary_color',
		'label'       => esc_html__( 'Secondary Color', 'structure' ),
		'description' => esc_html__( 'Choose the second most dominant theme color', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[1],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Third Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'heading_color',
		'label'       => esc_html__( 'Heading Color', 'structure' ),
		'description' => esc_html__( 'Choose color for your heading text', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[2],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Site Link Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'site_link_color',
		'label'       => esc_html__( 'Link Color', 'structure' ),
		'description' => esc_html__( 'Define styles for site link', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[3],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Site Link Hover Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'site_hover_link_color',
		'description' => esc_html__( 'Choose the hover text link color', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[4],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Body Background Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'body_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[5],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Body Text Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'body_text_color',
		'label'       => esc_html__( 'Body Text Color', 'structure' ),
		'description' => esc_html__( 'Choose the color for content of your site', 'structure' ),
		'section'     => $section,
		'default'     => $color_scheme[6],
		'output'      => array(
			'element'  => 'body.scheme',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Colors Settings
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'header_group_title_header_color_settings',
		'label'     => esc_html__( 'Header Colors', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Header Background Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for header area', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[7],
		'output'      => array(
			'element'  => '.scheme .header',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Text Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_text_color',
		'label'       => esc_html__( 'Text Color', 'structure' ),
		'description' => esc_html__( 'Choose the color for header text', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[8],
		'output'      => array(
			'element'  => '.scheme .header',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Top Area Colors Settings
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'header_group_title_header_top_area_color_settings',
		'label'     => esc_html__( 'Header Top Area Colors', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Header Top Area Link Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_top_area_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for header top area', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[21],
		'output'      => array(
			'element'  => '.scheme .top-area',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Top Area Link Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_top_area_link_color',
		'label'       => esc_html__( 'Link Color', 'structure' ),
		'description' => esc_html__( 'Choose the header top link color', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[9],
		'output'      => array(
			'element'  => '.scheme .top-area a',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Top Area Link Hover Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_top_area_hover_link_color',
		'description' => esc_html__( 'Choose the header top hover link color', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[10],
		'output'      => array(
			'element'  => '.scheme .top-area a:hover',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Header Top Area Text Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'header_top_area_text_color',
		'label'       => esc_html__( 'Text Color', 'structure' ),
		'description' => esc_html__( 'Choose the header top text color', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[11],
		'output'      => array(
			'element'  => '.scheme .top-area',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Search Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'search_group_title',
		'label'     => esc_html__( 'Search Button', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Search button Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'search_color',
		'label'       => esc_html__( 'Search Button Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for menu of your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[31],
		'output'      => array(
			'element'  => '.search-box i',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Mini Cart Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'mini_cart_group_title',
		'label'     => esc_html__( 'Mini Cart Button', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Cart button Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'cart_button_color',
		'label'       => esc_html__( 'Cart Button Color', 'structure' ),
		'description' => esc_html__( 'Choose the color for mini cart button', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[32],
		'output'      => array(
			'element'  => '.mini-cart .mini-cart__button .mini-cart-icon',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Cart number Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'cart_number_color',
		'label'       => esc_html__( 'Cart Number Color', 'structure' ),
		'description' => esc_html__( 'Choose the color for mini cart number', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[34],
		'output'      => array(
			'element'  => '.mini-cart .mini-cart__button .mini-cart-icon:after',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Cart number bg
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'cart_number_bg_color',
		'label'       => esc_html__( 'Cart Number Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for mini cart number', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[33],
		'output'      => array(
			'element'  => '.mini-cart .mini-cart__button .mini-cart-icon:after',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Menu Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'menu_group_title_menu',
		'label'     => esc_html__( 'Main Menu', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Menu Background Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for menu of your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[12],
		'output'      => array(
			'element'  => '.navigation,.header-preset-05 .nav',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Menu link text color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_text_color',
		'label'       => esc_html__( 'Link Color', 'structure' ),
		'description' => esc_html__( 'Menu link text color', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[13],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Menu hover link color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_text_color_hover',
		'description' => esc_html__( 'Menu hover link text color', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[14],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_border_right_color',
		'label'       => esc_html__( 'Border Right Color', 'structure' ),
		'description' => esc_html__( 'Border right color for menu item, only work with header preset 02,04', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[25],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_border_top_color',
		'label'       => esc_html__( 'Border Top Color', 'structure' ),
		'description' => esc_html__( 'Border top color for active menu item, only work with header preset 02', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[26],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'menu_border_bottom_color',
		'label'       => esc_html__( 'Border Bottom Color', 'structure' ),
		'description' => esc_html__( 'Border bottom color for active menu item, only work with header preset 02,04', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[27],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'mobile_menu_toggle_color',
		'label'       => esc_html__( 'Mobile Menu Toggle Color', 'structure' ),
		'description' => esc_html__( 'Color for mobile toggle menu', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[28],
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Sub Menu Group Title
	$controls[] = array(
		'type'      => 'group_title',
		'setting'   => 'footer_group_title_footer_color',
		'label'     => esc_html__( 'Footer Color', 'structure' ),
		'section'   => $section,
		'separator' => false,
		'priority'  => $priority ++
	);

	//Footer Background Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'footer_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the background color for footer of your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[16],
		'output'      => array(
			'element'  => '.scheme .footer',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Footer Heading Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'footer_heading_color',
		'label'       => esc_html__( 'Heading Color', 'structure' ),
		'description' => esc_html__( 'Choose the heading color for footer of your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[22],
		'output'      => array(
			'element'  => '.scheme .footer .widget-title',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Footer Text Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'footer_text_color',
		'label'       => esc_html__( 'Text Color', 'structure' ),
		'description' => esc_html__( 'Choose the text color for footer of your site', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[17],
		'output'      => array(
			'element'  => '.scheme .footer',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Footer Link Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'footer_link_color',
		'label'       => esc_html__( 'Link Color', 'structure' ),
		'description' => esc_html__( 'Choose the footer text link color', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[18],
		'output'      => array(
			'element'  => '.scheme .footer a',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Footer Link Hover Color
	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'footer_hover_link_color',
		'description' => esc_html__( 'Choose the footer hover text link color', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[19],
		'output'      => array(
			'element'  => '.scheme .footer a:hover',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	//Copyright Group Title
	$controls[] = array(
		'type'     => 'group_title',
		'setting'  => 'copyright_group_title',
		'label'    => esc_html__( 'Copyright', 'structure' ),
		'section'  => $section,
		'priority' => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'copyright_bg_color',
		'label'       => esc_html__( 'Background Color', 'structure' ),
		'description' => esc_html__( 'Choose the border color for copyright of your site', 'structure' ),
		'section'     => $section,
		'separator'   => false,
		'default'     => $color_scheme[23],
		'output'      => array(
			'element'  => '.scheme .copyright',
			'property' => 'background-color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'copyright_text_color',
		'label'       => esc_html__( 'Text Color', 'structure' ),
		'description' => esc_html__( 'Choose the text color for copyright section', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[24],
		'output'      => array(
			'element'  => '.scheme .copyright',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'copyright_link_color',
		'label'       => esc_html__( 'Link Color', 'structure' ),
		'description' => esc_html__( 'Choose the link color for copyright section', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[29],
		'output'      => array(
			'element'  => '.scheme .copyright a',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	$controls[] = array(
		'type'        => 'color',
		'setting'     => 'copyright_link_color_hover',
		'label'       => esc_html__( 'Link Color Hover', 'structure' ),
		'description' => esc_html__( 'Choose the link color on hover for copyright section', 'structure' ),
		'section'     => $section,
		'separator'   => true,
		'default'     => $color_scheme[30],
		'output'      => array(
			'element'  => '.scheme .copyright a:hover',
			'property' => 'color',
		),
		'transport'   => 'postMessage',
		'priority'    => $priority ++
	);

	return $controls;
}

add_filter( 'kirki/controls', 'register_controls_for_site_colors_section' );
