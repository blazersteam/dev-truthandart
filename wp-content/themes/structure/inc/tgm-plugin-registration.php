<?php
if (!function_exists('thememove_register_theme_plugins')):
	function thememove_register_theme_plugins() {
		$plugins = [
			[
				'name' => 'ThemeMove Core',
				'slug' => 'thememove-core',
				'source' => 'https://drive.google.com/uc?export=download&id=0By5Ytx4iv5jwaEtZMDJuQndWN0k',
				'version' => '1.3.5.1',
				'required' => true,
			],
			[
				'name' => 'WP Bakery Page Builder',
				'slug' => 'js_composer',
				'source' => 'https://bitbucket.org/digitalcreative/thememove-plugins/raw/f18605c73a80b804177355dee0ea68a96a805156/js_composer.zip',
				'version' => '6.0.4',
				'required' => false,
			],
			[
				'name' => 'Essential Grid',
				'slug' => 'essential-grid',
				'source' => 'https://bitbucket.org/digitalcreative/thememove-plugins/raw/417b91a8fcdfbc49a857cf8ef4aff76e67f1f35c/essential-grid.zip',
				'version' => '2.3.2',
				'required' => false,
			],
			[
				'name' => 'Revolution Slider',
				'slug' => 'revslider',
				'source' => 'https://bitbucket.org/digitalcreative/thememove-plugins/raw/f18605c73a80b804177355dee0ea68a96a805156/revslider.zip',
				'version' => '6.0.6',
				'required' => false,
			],
			[
				'name' => 'WP Job Manager',
				'slug' => 'wp-job-manager',
				'required' => false,
			],
			[
				'name' => 'Testimonials by WooThemes',
				'slug' => 'testimonials-by-woothemes',
				'required' => false,
			],
			[
				'name' => 'Projects by WooThemes',
				'slug' => 'projects-by-woothemes',
				'required' => true,
			],
			[
				'name' => 'WooCommerce',
				'slug' => 'woocommerce',
				'required' => false,
			],
			[
				'name' => 'Widget Logic',
				'slug' => 'widget-logic',
				'required' => false,
			],
			[
				'name' => 'MailChimp for WordPress',
				'slug' => 'mailchimp-for-wp',
				'required' => false,
			],
			[
				'name' => 'Contact Form 7',
				'slug' => 'contact-form-7',
				'required' => false,
			],
		];

		$config = [
			'id' => 'tgmpa',
			'default_path' => '',
			'menu' => 'tgmpa-install-plugins',
			'parent_slug' => 'themes.php',
			'capability' => 'edit_theme_options',
			'has_notices' => true,
			'dismissable' => true,
			'dismiss_msg' => '',
			'is_automatic' => false,
			'message' => '',
		];

		tgmpa($plugins, $config);
	}

	add_action('tgmpa_register', 'thememove_register_theme_plugins');
endif;
