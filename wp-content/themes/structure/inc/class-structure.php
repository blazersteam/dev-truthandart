<?php

class Structure {
	/**
	 * Top menu
	 */
	static function top_menu() {
		if ( class_exists( 'TM_Walker_Nav_Menu' ) && has_nav_menu( 'primary' ) ) {
			wp_nav_menu( array(
				'theme_location'  => 'primary',
				'menu_id'         => 'primary-menu',
				'container_class' => 'primary-menu',
				'walker'          => new TM_Walker_Nav_Menu
			) );
		} else {
			wp_nav_menu( array(
				'theme_location'  => 'primary',
				'menu_id'         => 'primary-menu',
				'container_class' => 'primary-menu',
			) );
		}
	}

	public static function get_gg_fonts_url($args = []) {
		$defaults = [
			'subsets' => 'latin,latin-ext'
		];

		$args = wp_parse_args($args, $defaults);
		$fonts = [];

		foreach ($args['font-families'] as $family => $params):
			$font_weight = $params['font-weight'];

			if ($params['translate'] === true) {
				$fonts[] = "$family:$font_weight";
			}
		endforeach;

		if (empty($fonts)) {
			return null;
		}

		$fonts_url = add_query_arg(
			[
				'family' => rawurlencode(implode('|', $fonts)),
				'subset' => rawurlencode($args['subsets'])
			],
			'https://fonts.googleapis.com/css'
		);

		return $fonts_url;
	}

}
