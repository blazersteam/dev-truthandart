<?php

function thememove_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'thememove_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
	) ) );
}

add_action( 'after_setup_theme', 'thememove_custom_header_setup' );
