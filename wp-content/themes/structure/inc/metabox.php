<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function cmb2_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}

	return true;
}

add_filter( 'cmb2_meta_boxes', 'thememove_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 *
 * @return array
 */
function thememove_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'thememove_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['page_metabox'] = array(
		'id'           => 'page_metabox',
		'title'        => esc_html__( 'Page Settings', 'structure' ),
		'object_types' => array( 'page' ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'       => array(
			array(
				'name'    => esc_html__( 'Bread Crumb', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for breadcrumb', 'structure' ),
				'id'      => $prefix . 'bread_crumb_enable',
				'type'    => 'select',
				'options' => array(
					'enable'  => esc_html__( 'Enable', 'structure' ),
					'disable' => esc_html__( 'Disable', 'structure' ),
				),
			),
			array(
				'name'    => esc_html__( 'Uncover Footer', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for uncover footer option', 'structure' ),
				'id'      => $prefix . 'uncover_enable',
				'type'    => 'select',
				'options' => array(
					'default' => esc_html__( 'Default', 'structure' ),
					'enable'  => esc_html__( 'Enable', 'structure' ),
					'disable' => esc_html__( 'Disable', 'structure' ),
				),
			),
			array(
				'name'    => esc_html__( 'Top Area', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for header top area', 'structure' ),
				'id'      => $prefix . 'header_top',
				'type'    => 'select',
				'options' => array(
					'default' => esc_html__( 'Default', 'structure' ),
					'enable'  => esc_html__( 'Enable', 'structure' ),
					'disable' => esc_html__( 'Disable', 'structure' ),
				),
			),
			array(
				'name'    => esc_html__( 'Sticky Header', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for sticky header', 'structure' ),
				'id'      => $prefix . 'sticky_header',
				'type'    => 'select',
				'options' => array(
					'default' => esc_html__( 'Default', 'structure' ),
					'enable'  => esc_html__( 'Enable', 'structure' ),
					'disable' => esc_html__( 'Disable', 'structure' ),
				),
			),
			array(
				'name' => esc_html__( 'Custom Logo', 'structure' ),
				'desc' => esc_html__( 'Upload an image or enter a URL for logo', 'structure' ),
				'id'   => $prefix . 'custom_logo',
				'type' => 'file',
			),
			array(
				'name'    => esc_html__( 'Header Presets', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for header presets', 'structure' ),
				'id'      => $prefix . 'header_preset',
				'type'    => 'select',
				'options' => array(
					'default'          => esc_html__( 'Default', 'structure' ),
					'header-preset-01' => esc_html__( 'Preset 01', 'structure' ),
					'header-preset-02' => esc_html__( 'Preset 02', 'structure' ),
					'header-preset-03' => esc_html__( 'Preset 03', 'structure' ),
					'header-preset-04' => esc_html__( 'Preset 04', 'structure' ),
					'header-preset-05' => esc_html__( 'Preset 05', 'structure' ),
					'header-preset-06' => esc_html__( 'Preset 06', 'structure' ),
					'header-preset-07' => esc_html__( 'Preset 07', 'structure' ),
					'header-preset-08' => esc_html__( 'Preset 08', 'structure' ),
				),
			),
			array(
				'name'    => esc_html__( 'Color Scheme', 'structure' ),
				'desc'    => esc_html__( 'Custom settings for color scheme', 'structure' ),
				'id'      => $prefix . 'color_scheme',
				'type'    => 'select',
				'options' => array(
					'default'  => esc_html__( 'Default', 'structure' ),
					'scheme1'  => esc_html__( 'Color Scheme for Header Preset 01', 'structure' ),
					'scheme2'  => esc_html__( 'Color Scheme for Header Preset 02', 'structure' ),
					'scheme3'  => esc_html__( 'Color Scheme for Header Preset 03', 'structure' ),
					'scheme4'  => esc_html__( 'Color Scheme for Header Preset 04', 'structure' ),
					'scheme5'  => esc_html__( 'Color Scheme for Header Preset 05', 'structure' ),
					'scheme6'  => esc_html__( 'Color Scheme for Header Preset 06', 'structure' ),
					'scheme7'  => esc_html__( 'Color Scheme for Header Preset 07', 'structure' ),
					'scheme8'  => esc_html__( 'Color Scheme for Home V2 Default', 'structure' ),
					'scheme9'  => esc_html__( 'Color Scheme for Home V2 Black', 'structure' ),
					'scheme10' => esc_html__( 'Color Scheme for Home V2 White', 'structure' ),
				),
			),
			array(
				'name'    => esc_html__( 'Page Layout', 'structure' ),
				'desc'    => esc_html__( 'Choose a layout you want', 'structure' ),
				'id'      => $prefix . 'page_layout_private',
				'type'    => 'select',
				'options' => array(
					'default'         => esc_html__( 'Default', 'structure' ),
					'full-width'      => esc_html__( 'Full width', 'structure' ),
					'content-sidebar' => esc_html__( 'Content-Sidebar', 'structure' ),
					'sidebar-content' => esc_html__( 'Sidebar-Content', 'structure' ),
				),
			),
			array(
				'name' => 'Disable Title',
				'desc' => 'Check this box to disable the title of the page',
				'id'   => $prefix . 'disable_title',
				'type' => 'checkbox'
			),
			array(
				'name' => esc_html__( 'Title Background', 'structure' ),
				'desc' => esc_html__( 'Upload an image or enter a URL for heading title', 'structure' ),
				'id'   => $prefix . 'heading_image',
				'type' => 'file',
			),
			array(
				'name' => 'Alternative Title',
				'desc' => 'Enter your alternative title here',
				'id'   => $prefix . 'alt_title',
				'type' => 'textarea_small'
			),
			array(
				'name' => 'Disable Parallax',
				'desc' => 'Check this box to disable parallax effect for heading title',
				'id'   => $prefix . 'disable_parallax',
				'type' => 'checkbox'
			),
			array(
				'name' => 'Contact Adress',
				'desc' => 'Enter your address here and it will display on map in contact page',
				'id'   => $prefix . 'contact_address',
				'type' => 'text'
			),
			array(
				'name' => 'Custom Class',
				'desc' => 'Enter custom class for this page',
				'id'   => $prefix . 'custom_class',
				'type' => 'text'
			),
		),
	);

	return $meta_boxes;
}
