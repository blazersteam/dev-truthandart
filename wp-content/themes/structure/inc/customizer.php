<?php
/**
 * ThemeMove Theme Customizer
 *
 * @package ThemeMove
 */

/**
 * ============================================================================
 * Load customizer configs
 * ============================================================================
 */
function customizer_config() {
	$args = array(
		'url_path'      => STRUCTURE_THEME_ROOT . '/core/customizer/',
		'stylesheet_id' => 'thememove-main',
	);

	return $args;
}

add_filter( 'kirki/config', 'customizer_config' );

function structure_controls_init( $controls ) {
	if ( ! is_array( $controls ) ) {
		$controls = array();
	}

	return $controls;
}

add_filter( 'kirki/controls', 'structure_controls_init');

/**
 * ============================================================================
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * ============================================================================
 */
function thememove_customize_preview_js() {
	wp_enqueue_script( 'thememove_customizer', STRUCTURE_THEME_ROOT . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}

add_action( 'customize_preview_init', 'thememove_customize_preview_js' );

/**
 * ============================================================================
 * Include panels/sections
 * ============================================================================
 */
require_once get_template_directory() . '/inc/customizer/section__options.php';
require_once get_template_directory() . '/inc/customizer/section__typography.php';
require_once get_template_directory() . '/inc/customizer/section__colors.php';
require_once get_template_directory() . '/inc/customizer/section__menu.php';
require_once get_template_directory() . '/inc/customizer/section__header.php';
require_once get_template_directory() . '/inc/customizer/section__footer.php';
require_once get_template_directory() . '/inc/customizer/section__post.php';
require_once get_template_directory() . '/inc/customizer/section__customcode.php';
require_once get_template_directory() . '/inc/customizer/section__project.php';
require_once get_template_directory() . '/inc/customizer/section__job.php';

/**
 * ============================================================================
 * Color Scheme Settings
 * ============================================================================
 */
if ( ! function_exists( 'thememove_get_color_scheme' ) ) :
	function thememove_get_color_scheme() {
		$color_scheme_option = get_theme_mod( 'site_color_scheme', 'scheme1' );
		$color_schemes       = thememove_get_color_schemes();
		global $thememove_color_scheme;

		if ( $thememove_color_scheme == 'default' ) {
			return $color_schemes[ $color_scheme_option ]['colors'];
		} elseif ( $thememove_color_scheme != 'default' && $thememove_color_scheme != '' ) {
			return $color_schemes[ $thememove_color_scheme ]['colors'];
		} elseif ( array_key_exists( $color_scheme_option, $color_schemes ) ) {
			return $color_schemes[ $color_scheme_option ]['colors'];
		}

		return $color_schemes['scheme1']['colors'];
	}
endif; // thememove_get_color_scheme

if ( ! function_exists( 'thememove_get_color_scheme_choices' ) ) :
	function thememove_get_color_scheme_choices() {
		$color_schemes                = thememove_get_color_schemes();
		$color_scheme_control_options = array();

		foreach ( $color_schemes as $color_scheme => $value ) {
			$color_scheme_control_options[ $color_scheme ] = $value['label'];
		}

		return $color_scheme_control_options;
	}
endif;
