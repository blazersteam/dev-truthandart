<?php

add_filter( 'thememove_import_demos', 'tm_structure_import_demos' );

function tm_structure_import_demos() {
	return array(
		'thememove01' => array(
			'screenshot' => STRUCTURE_THEME_ROOT . '/screenshot.jpg',
			'name'       => STRUCTURE_PARENT_THEME_NAME
		)
	);
}

add_filter( 'thememove_import_style', 'tm_structure_import_style' );

function tm_structure_import_style() {
	return array(
		'title_color'  => '#222222',
		'link_color'   => '#FBD232',
		'notice_color' => '#FBD232',
		'logo'         => STRUCTURE_THEME_ROOT . '/images/logo.png'
	);
}

add_filter('thememove_import_package_url', 'tm_structure_import_package_url');

function tm_structure_import_package_url() {
	return 'https://api.insightstud.io/import/structure/structure-thememove01.zip';
}
