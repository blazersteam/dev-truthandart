<?php
/**
 * ThemeMove functions and definitions
 *
 * @package ThemeMove
 */

/**
 * ============================================================================
 * Set the content width based on the theme's design and stylesheet.
 * ============================================================================
 */
if (!isset($content_width)) {
	$content_width = 640; /* pixels */
}

/**
 * ============================================================================
 * Thememove only works in WordPress 3.9 or later.
 * ============================================================================
 */
if (version_compare($GLOBALS['wp_version'], '3.9', '<')) {
	require get_template_directory() . '/inc/back-compat.php';
}

/**
 * ============================================================================
 * Define Constants
 * ============================================================================
 */

define('STRUCTURE_THEME_ROOT', esc_url(get_template_directory_uri()));
define('STRUCTURE_PARENT_THEME_NAME', wp_get_theme(get_template())->get('Name'));
define('STRUCTURE_PARENT_THEME_VERSION', wp_get_theme(get_template())->get('Version'));

require_once get_template_directory() . '/inc/variables.php';

if (!function_exists('thememove_setup')):
	/**
	 * ============================================================================
	 * Sets up theme defaults and registers support for various WordPress features.
	 * ============================================================================
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thememove_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ThemeMove, use a find and replace
		 * to change 'thememove' to the name of your theme in all the template files
		 */
		load_theme_textdomain('structure', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support('post-thumbnails');
		add_image_size('project-single', 1170, 600, true);
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus([
			'primary' => esc_html__('Primary Menu', 'structure'),
			'social' => esc_html__('Social Profile Links', 'structure'),
		]);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', ['search-form', 'comment-form', 'comment-list', 'gallery', 'caption']);

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support('post-formats', ['aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery']);

		// Setup the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('thememove_custom_background_args', [
			'default-color' => 'f7f7f7',
			'default-image' => 'http://structure.thememove.com/data/images/notebook.png',
		]));

		//support woocommerce
		add_theme_support('woocommerce');
		add_theme_support('wc-product-gallery-zoom');
		add_theme_support('wc-product-gallery-lightbox');
		add_theme_support('wc-product-gallery-slider');

		//support gutenberg
		add_theme_support('align-wide');
		add_theme_support('wp-block-styles');
		add_theme_support('editor-styles');
		add_theme_support('responsive-embeds');

		add_filter('essgrid_set_cpt', '__return_true', 10, 2);
	}
endif; // thememove_setup
add_action('after_setup_theme', 'thememove_setup');

/**
 * ============================================================================
 * Register widget area.
 * ============================================================================
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function thememove_widgets_init() {
	register_sidebar([
		'name' => esc_html__('Sidebar', 'structure'),
		'id' => 'sidebar',
		'description' => esc_html__('Sidebar', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Sidebar for shop', 'structure'),
		'id' => 'sidebar-shop',
		'description' => esc_html__('Sidebar for shop', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Top Slider', 'structure'),
		'id' => 'top-slider',
		'description' => esc_html__('Top Slider', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Top Widget Area', 'structure'),
		'id' => 'top-area',
		'description' => esc_html__('Top Widget Area', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Header Right Widget Area', 'structure'),
		'id' => 'header-right',
		'description' => esc_html__('Header Right Widget Area', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Footer 1 Widget Area', 'structure'),
		'id' => 'footer',
		'description' => esc_html__('Footer 1 Widget Area', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Footer 2 Widget Area', 'structure'),
		'id' => 'footer2',
		'description' => esc_html__('Footer 2 Widget Area', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	register_sidebar([
		'name' => esc_html__('Footer 3 Widget Area', 'structure'),
		'id' => 'footer3',
		'description' => esc_html__('Footer 3 Widget Area', 'structure'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	]);
	if (class_exists('SitePress') || class_exists('Polylang')) {
		register_sidebar([
			'name' => esc_html__('Language Widget Area', 'structure'),
			'id' => 'lang-area',
			'description' => esc_html__('Language Widget Area', 'structure'),
			'before_widget' => '<aside id="%1$s" class="widget widget-language %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		]);
	}
	if (class_exists('WP_Job_Manager')) {
		register_sidebar([
			'name' => esc_html__('Job Widget Area', 'structure'),
			'id' => 'job-area',
			'description' => esc_html__('Job Widget Area', 'structure'),
			'before_widget' => '<aside id="%1$s" class="widget widget-job %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		]);
	}
}

add_action('widgets_init', 'thememove_widgets_init');

/**
 * ============================================================================
 * Enqueue scripts and styles.
 * ===========================================================================
 */
function thememove_scripts() {
	wp_enqueue_style('thememove-style', STRUCTURE_THEME_ROOT . '/style.css');
	wp_enqueue_style('thememove-main', STRUCTURE_THEME_ROOT . '/css/main-style.css');
	wp_enqueue_style('font-awesome', STRUCTURE_THEME_ROOT . '/inc/fontawesome/css/all.css');
	wp_enqueue_style('vc_google_fonts_playfair_display', Structure::get_gg_fonts_url([
		'font-families' => [
			'Playfair Display' => [
				'font-weight' => '700i, 900i',
				'translate' => true,
			],
		],
	]));

	if (get_theme_mod('page_transition') === 'type1') {
		wp_enqueue_script('nprogress', STRUCTURE_THEME_ROOT . '/js/nprogress.js', ['jquery'], null, true);
	} elseif (get_theme_mod('page_transition') === 'type2') {
		wp_enqueue_script('thememove-js-animsition', STRUCTURE_THEME_ROOT . '/js/jquery.animsition.min.js', ['jquery'],
			null, true);
	}
	wp_enqueue_script('thememove-js-stellar', STRUCTURE_THEME_ROOT . '/js/jquery.stellar.min.js', ['jquery'], null,
		true);
	wp_enqueue_script('matchHeight', STRUCTURE_THEME_ROOT . '/js/jquery.matchHeight.js', [], null, true);
	wp_enqueue_script('thememove-js-owl-carousel', STRUCTURE_THEME_ROOT . '/js/owl.carousel.min.js');
	if (get_theme_mod('enable_smooth_scroll', enable_smooth_scroll)) {
		wp_enqueue_script('thememove-js-smooth-scroll', STRUCTURE_THEME_ROOT . '/js/smoothscroll.js');
	}

	wp_enqueue_style('jquery.menu-css', STRUCTURE_THEME_ROOT . '/js/jQuery.mmenu/css/jquery.mmenu.all.css');
	wp_enqueue_script('jquery.menu-js', STRUCTURE_THEME_ROOT . '/js/jQuery.mmenu/js/jquery.mmenu.all.min.js', [], null,
		true);

	global $thememove_sticky_header;
	if ((get_theme_mod('header_sticky_enable',
				header_sticky_enable) || $thememove_sticky_header === 'enable') && $thememove_sticky_header !== 'disable') {
		wp_enqueue_script('head-room-jquery', STRUCTURE_THEME_ROOT . '/js/jQuery.headroom.min.js');
		wp_enqueue_script('head-room', STRUCTURE_THEME_ROOT . '/js/headroom.min.js');
	}
	wp_enqueue_script('magnific', STRUCTURE_THEME_ROOT . '/js/jquery.magnific-popup.min.js');
	wp_enqueue_script('counterup', STRUCTURE_THEME_ROOT . '/js/jquery.counterup.min.js');
	wp_enqueue_script('waypoints', STRUCTURE_THEME_ROOT . '/js/waypoints.min.js');
	wp_enqueue_script('thememove-js-main', STRUCTURE_THEME_ROOT . '/js/main.js', [], null, true);
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}

add_action('wp_enqueue_scripts', 'thememove_scripts');

add_action('enqueue_block_editor_assets', function () {
	wp_enqueue_style('editor-google-font', Structure::get_gg_fonts_url([
		'font-families' => [
			'Lato' => [
				'font-weight' => '400,400i,700,700i',
				'translate' => true,
			],
			'Montserrat' => [
				'font-weight' => '400,400i,600,600i,700,700i',
				'translate' => true,
			],
		],
	]));
	wp_enqueue_style('editor-style', STRUCTURE_THEME_ROOT . '/editor-style.css');
});

/**
 * ============================================================================
 * Loading components.
 * ============================================================================
 */
require_once get_template_directory() . '/core/customizer/kirki.php';
require_once get_template_directory() . '/inc/class-structure.php';
require_once get_template_directory() . '/inc/oneclick.php';
require_once get_template_directory() . '/inc/extras.php';
require_once get_template_directory() . '/inc/custom-header.php';
require_once get_template_directory() . '/inc/template-tags.php';
require_once get_template_directory() . '/inc/jetpack.php';
require_once get_template_directory() . '/inc/customizer.php';
require_once get_template_directory() . '/inc/metabox.php';
require_once get_template_directory() . '/inc/custom-css.php';
require_once get_template_directory() . '/inc/custom-js.php';

/**
 * ============================================================================
 * Requirement Plugins
 * ============================================================================
 */
require_once get_template_directory() . '/inc/tgm-plugin-activation.php';
require_once get_template_directory() . '/inc/tgm-plugin-registration.php';

/**
 * ============================================================================
 * Auto Update Theme
 * ============================================================================
 */
require_once get_template_directory() . '/wp-updates-theme.php';
new WPUpdatesThemeUpdater_1276('http://wp-updates.com/api/2/theme', basename(get_template_directory()));


/**
 * ============================================================================
 * General setups
 * ============================================================================
 */
// Remove admin notification of Projects
if (class_exists('Projects_Admin')) {
	global $projects;
	remove_action('admin_notices', [$projects->admin, 'configuration_admin_notice']);
}

add_filter('projects_enqueue_styles', '__return_false');
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

add_filter('loop_shop_per_page', function () {
	return 12;
}, 20);

add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

// Extend VC
if (class_exists('WPBakeryVisualComposerAbstract')) {
	function requireVcExtend() {
		require_once get_template_directory() . '/inc/vc-extend.php';
	}

	add_action('init', 'requireVcExtend', 2);
}

// Remove admin notification of Projects
if (class_exists('Projects')) {
	global $projects;
	remove_action('admin_notices', [$projects->admin, 'configuration_admin_notice']);
}

//is_tree
if (!function_exists('is_tree')) {
	function is_tree($pid) {
		global $post;

		return is_page() && ($post->post_parent === $pid || is_page($pid));
	}
}

function tm_bread_crumb_project() {
	?>
	<ul class="tm_bread_crumb">
		<li class="level-1 top"><a
				href="<?php echo home_url(); ?>"><?php echo esc_html__('Home', 'structure'); ?></a>
		</li>
		<?php if (tm_get_page_url_by_slug(get_theme_mod('project_archive_page_slug', 'all-projects')) != '') { ?>
			<li class="level-2 sub"><a
					href="<?php echo tm_get_page_url_by_slug(get_theme_mod('project_archive_page_slug',
						'all-projects')); ?>"><?php echo esc_html__('Our Projects', 'structure'); ?></a>
			</li>
		<?php } ?>
		<?php
		$pcterms = wp_get_post_terms(get_the_ID(), 'project-category');
		if (count($pcterms) > 0) {
			foreach ($pcterms as $pcterm) {
				echo '<li class="level-3 sub"><a href="' . tm_get_page_url_by_slug(get_theme_mod('project_archive_page_slug',
						'all-projects')) . '#' . $pcterm->slug . '">' . $pcterm->name . '</a></li>';
			}
		}
		?>
		<li class="level-4 sub tail current"><?php the_title(); ?></li>
	</ul>
	<?php
}

function tm_bread_crumb_job() {
	?>
	<ul class="tm_bread_crumb">
		<li class="level-1 top"><a
				href="<?php echo home_url(); ?>"><?php echo esc_html__('Home', 'structure'); ?></a>
		</li>
		<?php if (tm_get_page_url_by_slug(get_theme_mod('job_archive_page_slug', 'company/careers')) != '') { ?>
			<li class="level-2 sub"><a
					href="<?php echo tm_get_page_url_by_slug(get_theme_mod('job_archive_page_slug',
						'company/careers')); ?>"><?php echo esc_html__('Careers', 'structure'); ?></a>
			</li>
		<?php } ?>
		<li class="level-4 sub tail current"><?php the_title(); ?></li>
	</ul>å
	<?php
}

function tm_get_page_url_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return esc_url(get_page_link($page->ID));
	} else {
		return '';
	}
}

add_filter('image_downsize', function ($out, $id, $size) {
	global $all_image_sizes;

	if (!isset($all_image_sizes)) {
		global $_wp_additional_image_sizes;

		$all_image_sizes = [];
		$interim_sizes = get_intermediate_image_sizes();

		foreach ($interim_sizes as $size_name) {
			if (in_array($size_name, ['thumbnail', 'medium', 'large'], true)) {
				$all_image_sizes[$size_name]['width'] = get_option($size_name . '_size_w');
				$all_image_sizes[$size_name]['height'] = get_option($size_name . '_size_h');
				$all_image_sizes[$size_name]['crop'] = (bool)get_option($size_name . '_crop');
			} elseif (isset($_wp_additional_image_sizes[$size_name])) {
				$all_image_sizes[$size_name] = $_wp_additional_image_sizes[$size_name];
			}
		}
	}

	$all_sizes = $all_image_sizes;

	$image_data = wp_get_attachment_metadata($id);

	if (!is_array($image_data)) {
		return false;
	}

	if (is_string($size)) {
		if (empty($all_sizes[$size])) {
			return false;
		}

		if (!empty($image_data['sizes'][$size]) && !empty($all_sizes[$size])) {
			if (
				$all_sizes[$size]['width'] === $image_data['sizes'][$size]['width'] &&
				$all_sizes[$size]['height'] === $image_data['sizes'][$size]['height']
			) {
				return false;
			}

			if (!empty($image_data['sizes'][$size]['width_query']) && !empty($image_data['sizes'][$size]['height_query'])) {
				if (
					$image_data['sizes'][$size]['width_query'] === $all_sizes[$size]['width'] &&
					$image_data['sizes'][$size]['height_query'] === $all_sizes[$size]['height']
				) {
					return false;
				}
			}
		}

		$resized = image_make_intermediate_size(
			get_attached_file($id),
			$all_sizes[$size]['width'],
			$all_sizes[$size]['height'],
			$all_sizes[$size]['crop']
		);

		if (!$resized) {
			return false;
		}

		$image_data['sizes'][$size] = $resized;

		$image_data['sizes'][$size]['width_query'] = $all_sizes[$size]['width'];
		$image_data['sizes'][$size]['height_query'] = $all_sizes[$size]['height'];

		wp_update_attachment_metadata($id, $image_data);

		$att_url = wp_get_attachment_url($id);

		return [dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true];
	} elseif (is_array($size)) {
		$image_path = get_attached_file($id);
		$crop = array_key_exists(2, $size) ? $size[2] : true;
		$new_width = $size[0];
		$new_height = $size[1];

		if (!$crop) {
			if (class_exists('Jetpack') && Jetpack::is_module_active('photon')) {
				add_filter('jetpack_photon_override_image_downsize', '__return_true');
				$true_data = wp_get_attachment_image_src($id, 'large');
			} else {
				$true_data = wp_get_attachment_image_src($id, 'large');
			}

			if ($true_data[1] > $true_data[2]) {
				$ratio = $true_data[1] / $size[0];
				$new_height = round($true_data[2] / $ratio);
				$new_width = $size[0];
			} else {
				$ratio = $true_data[2] / $size[1];
				$new_height = $size[1];
				$new_width = round($true_data[1] / $ratio);
			}
		}

		$image_ext = pathinfo($image_path, PATHINFO_EXTENSION);
		$image_path = preg_replace(
			'/^(.*)\.' . $image_ext . '$/',
			sprintf('$1-%sx%s.%s', $new_width, $new_height, $image_ext),
			$image_path
		);
		$att_url = wp_get_attachment_url($id);

		if (file_exists($image_path)) {
			return [dirname($att_url) . '/' . basename($image_path), $new_width, $new_height, $crop];
		}

		$resized = image_make_intermediate_size(get_attached_file($id), $size[0], $size[1], $crop);

		$image_data = wp_get_attachment_metadata($id);

		$image_data['sizes'][$size[0] . 'x' . $size[1]] = $resized;

		wp_update_attachment_metadata($id, $image_data);

		if (!$resized) {
			return false;
		}

		return [dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], $crop];
	}

	return false;
}
	, 10, 3);

add_action('pre_get_posts', static function (WP_Query $query) {
	if (!is_admin() && $query->is_main_query()) {
		if (is_post_type_archive('project') || is_tax('project_category')) {
			$query->set('posts_per_page', '8');
		}
	}
});
