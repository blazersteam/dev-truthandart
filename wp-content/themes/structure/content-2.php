<?php
/**
 * @package ThemeMove
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-thumb">
			<div class="dates">
				<span class="month"><?php the_time( 'F' ); ?></span>
				<span class="date"><?php the_time( 'd' ); ?></span>
				<span class="year"><?php the_time( 'Y' ); ?></span>
				<span
					class="comments-counts"><span><?php comments_number( '0', '1', '%' ); ?></span><?php comments_number( 'Comment', esc_html__( 'Comment', 'structure' ), esc_html__( 'Comments', 'structure' ) ); ?></span>
			</div>
			<?php the_post_thumbnail([848, 450]); ?>
		</div>
	<?php } ?>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header>
	<!-- .entry-header -->
	<?php if ('post' === get_post_type()): ?>
		<div class="entry-meta">
			<span class="author vcard"><i
					class="fa fa-user"></i> <?php echo esc_html__( 'Posted by ', 'structure' ) . get_the_author(); ?></span>
			<span class="categories-links">
				<i class="fa fa-folder"></i>
				<?php echo esc_html__('In ', 'structure') . get_the_category_list(esc_html__(', ', 'structure')); ?>
			</span>
		</div><!-- .entry-meta -->
	<?php endif; ?>

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>
	<!-- .entry-content -->
	<div class="row">
		<div class="col-sm-6">
			<a class="btn read-more" href="<?php the_permalink(); ?>">
				<?php echo esc_html__('Continue Reading', 'structure'); ?>
			</a>
		</div>
		<div class="col-sm-6">
			<div class="share">
				<span><i class="fa fa-share-alt"></i> <?php echo esc_html__( 'Share: ', 'structure' ); ?></span>
				<span>
					<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
						<i class="fa fa-facebook"></i>
					</a>
				</span>
				<span>
					<a target="_blank"
					   href="http://twitter.com/share?text=<?php echo rawurlencode(get_the_title()); ?>&url=<?php echo rawurlencode(get_the_permalink()); ?>">
						<i class="fa fa-twitter"></i>
					</a>
				</span>
				<span>
					<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
						<i class="fa fa-google-plus"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

</article><!-- #post-## -->
