<?php


class Kirki_Controls extends Kirki {

	function add_control( $wp_customize, $control ) {
		// The value of this control
		if ( ! isset( $control['default'] ) ) {
			$control['default'] = '';
		}

		$control['label']       = isset( $control['label'] ) ? $control['label'] : '';
		$control['settings']    = $control['setting'];
		$control['description'] = isset( $control['description'] ) ? $control['description'] : null;
		$control['subtitle']    = isset( $control['subtitle'] ) ? $control['subtitle'] : '';
		$control['separator']   = isset( $control['separator'] ) ? $control['separator'] : false;
		$control['required']    = isset( $control['required'] ) ? $control['required'] : array();
		$control['transport']   = isset( $control['transport'] ) ? $control['transport'] : 'refresh';
		$control['default']     = 'sortable' == $control['type'] ? maybe_serialize( $control['default'] ) : $control['default'];

		if ( 'background' != $control['type'] ) {

			$control_class = 'Kirki_Customize_' . ucfirst( $control['type'] ) . '_Control';
			$control_class = ( 'group_title' == $control['type'] ) ? 'Kirki_Customize_Group_Title_Control' : $control_class;
			$wp_customize->add_control( new $control_class( $wp_customize, $control['setting'] . '_opacity', $control ) );

		} else {

			/**
			 * The background control is a multi-control element
			 * so it requires extra steps to be created
			 */
			$wp_customize->add_control( new Kirki_Customize_Color_Control( $wp_customize, $control['setting'] . '_color', array(
				'label'       => isset( $control['label'] ) ? $control['label'] : '',
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_color',
				'priority'    => $control['priority'],
				'description' => $control['description'],
				'subtitle'    => esc_html__( 'Background Color', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			$wp_customize->add_control( new Kirki_Customize_Image_Control( $wp_customize, $control['setting'] . '_image', array(
				'label'       => null,
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_image',
				'priority'    => $control['priority'] + 1,
				'description' => null,
				'subtitle'    => esc_html__( 'Background Image', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			$wp_customize->add_control( new Kirki_Customize_Select_Control( $wp_customize, $control['setting'] . '_repeat', array(
				'label'       => null,
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_repeat',
				'priority'    => $control['priority'] + 2,
				'choices'     => array(
					'no-repeat' => esc_html__( 'No Repeat', 'structure' ),
					'repeat'    => esc_html__( 'Repeat All', 'structure' ),
					'repeat-x'  => esc_html__( 'Repeat Horizontally', 'structure' ),
					'repeat-y'  => esc_html__( 'Repeat Vertically', 'structure' ),
					'inherit'   => esc_html__( 'Inherit', 'structure' )
				),
				'description' => null,
				'subtitle'    => esc_html__( 'Background Repeat', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			$wp_customize->add_control( new Kirki_Customize_Radio_Control( $wp_customize, $control['setting'] . '_size', array(
				'label'       => null,
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_size',
				'priority'    => $control['priority'] + 3,
				'choices'     => array(
					'inherit' => esc_html__( 'Inherit', 'structure' ),
					'cover'   => esc_html__( 'Cover', 'structure' ),
					'contain' => esc_html__( 'Contain', 'structure' ),
				),
				'description' => null,
				'mode'        => 'buttonset',
				'subtitle'    => esc_html__( 'Background Size', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			$wp_customize->add_control( new Kirki_Customize_Radio_Control( $wp_customize, $control['setting'] . '_attach', array(
				'label'       => null,
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_attach',
				'priority'    => $control['priority'] + 4,
				'choices'     => array(
					'inherit' => esc_html__( 'Inherit', 'structure' ),
					'fixed'   => esc_html__( 'Fixed', 'structure' ),
					'scroll'  => esc_html__( 'Scroll', 'structure' ),
				),
				'description' => null,
				'mode'        => 'buttonset',
				'subtitle'    => esc_html__( 'Background Attachment', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			$wp_customize->add_control( new Kirki_Customize_Select_Control( $wp_customize, $control['setting'] . '_position', array(
				'label'       => null,
				'section'     => $control['section'],
				'settings'    => $control['setting'] . '_position',
				'priority'    => $control['priority'] + 5,
				'choices'     => array(
					'left-top'      => esc_html__( 'Left Top', 'structure' ),
					'left-center'   => esc_html__( 'Left Center', 'structure' ),
					'left-bottom'   => esc_html__( 'Left Bottom', 'structure' ),
					'right-top'     => esc_html__( 'Right Top', 'structure' ),
					'right-center'  => esc_html__( 'Right Center', 'structure' ),
					'right-bottom'  => esc_html__( 'Right Bottom', 'structure' ),
					'center-top'    => esc_html__( 'Center Top', 'structure' ),
					'center-center' => esc_html__( 'Center Center', 'structure' ),
					'center-bottom' => esc_html__( 'Center Bottom', 'structure' ),
				),
				'description' => null,
				'subtitle'    => esc_html__( 'Background Position', 'structure' ),
				'required'    => $control['required'],
				'transport'   => $control['transport']
			) ) );

			if ( false != $control['default']['opacity'] ) {
				$wp_customize->add_control( new Kirki_Customize_Slider_Control( $wp_customize, $control['setting'] . '_opacity', array(
					'label'       => null,
					'section'     => $control['section'],
					'settings'    => $control['setting'] . '_opacity',
					'priority'    => $control['priority'] + 6,
					'choices'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'description' => null,
					'subtitle'    => esc_html__( 'Background Opacity', 'structure' ),
					'required'    => $control['required'],
					'transport'   => $control['transport']
				) ) );

			}

		}

	}

}
